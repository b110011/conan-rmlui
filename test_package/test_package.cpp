#include <RmlUi/Core.h>
#include <RmlUi/Debugger.h>

class FontEngineInterfaceImpl final
	: public Rml::Core::FontEngineInterface
{};

class RenderInterfaceImpl final
	: public Rml::Core::RenderInterface
{
public:

	void RenderGeometry(Rml::Core::Vertex*, int, int*, int, Rml::Core::TextureHandle, const Rml::Core::Vector2f&) override {}
	
	void EnableScissorRegion(bool) override {}
	
	void SetScissorRegion(int, int, int, int) override {}
	
};

class SystemInterfaceImpl final
	: public Rml::Core::SystemInterface
{
public:

	double GetElapsedTime() override { return 0.0; }
	
};

int main(int argc, char** argv)
{
	// Initialize the window and graphics API being used, along with your game or application.
	
	/* ... */

	// Instantiate the interfaces to RmlUi.
	FontEngineInterfaceImpl font_engine_interface;
	RenderInterfaceImpl render_interface;
	SystemInterfaceImpl system_interface;

	// Begin by installing the custom interfaces.
	Rml::Core::SetFontEngineInterface(&font_engine_interface);
	Rml::Core::SetRenderInterface(&render_interface);
	Rml::Core::SetSystemInterface(&system_interface);

	// Now we can initialize RmlUi.
	Rml::Core::Initialise();
	
	// Create a context next.
	auto* context = Rml::Core::CreateContext("main", Rml::Core::Vector2i(100, 100));
	if (!context)
	{
		Rml::Core::Shutdown();
		return EXIT_FAILURE;
	}

	// Process user input and render your game or application.

	/* ... */

	// Shutting down RmlUi releases all its resources, including elements, documents, and contexts.
	Rml::Core::Shutdown();

	// It is now safe to destroy the custom interfaces previously passed to RmlUi.
	return EXIT_SUCCESS;
}
