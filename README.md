# conan-rmlui

[Conan.io](https://conan.io) package for [RmlUi](https://github.com/mikke89/RmlUi)

RmlUi is the C++ user interface package based on the HTML and CSS standards, designed as a complete solution for any project's interface needs. It is a fork of the [libRocket](https://github.com/libRocket/libRocket) project, introducing new features, bug fixes, and performance improvements.

## Building the package

Install the Conan package manager and the Conan package tools with pip

```bash
$ pip install conan_package_tools
```

Then build the package with

```bash
$ python build.py
```

## Uploading the package

```bash
$ conan upload RmlUi/3.3@b110011/stable
```

## Using the package

### Installation

You can manually install this package to your local cache by running

```bash
$ conan install RmlUi/3.3@b110011/stable
```

If you handle multiple dependencies in your project it's better to add a *conanfile.txt*

```yaml
[requires]
RmlUi/3.3@b110011/stable

[generators]
cmake
```

Then you can install all requirements for your project by running

```bash
$ conan install .
```

For further information on how to use Conan packages see the [Conan documentation](http://docs.conan.io/)

### Options

| Conan                       | CMAKE                       | Revers   | Default  |
| --------------------------- | --------------------------- | -------- | -------- |
| enable_tracy_profiling      | ENABLE_TRACY_PROFILING      | False    | False    |
| shared                      | BUILD_SHARED_LIBS           | False    | False    |
| use_precompiled_headers     | ENABLE_PRECOMPILED_HEADERS  | False    | **True** |
| with_default_font_interface | NO_FONT_INTERFACE_DEFAULT   | **True** | False    |
| with_lua_bindings           | BUILD_LUA_BINDINGS          | False    | False    |
| with_matrix_row_major       | MATRIX_ROW_MAJOR            | False    | False    |
| with_rtti_and_exceptions    | DISABLE_RTTI_AND_EXCEPTIONS | **True** | **True** |
| with_thirdparty_containers  | NO_THIRDPARTY_CONTAINERS    | **True** | **True** |

Please note, you can find the full description of all available CMake options token on the official [RmlUI](https://github.com/mikke89/RmlUi) [documentation page](https://mikke89.github.io/RmlUiDoc/pages/cpp_manual/building_with_cmake.html).

Please note, all Conan options marked as reversed will invert their values to CMAKE options. For example `with_rtti_and_exceptions=True` will be passed to the CMake as `DISABLE_RTTI_AND_EXCEPTIONS=False`.
